trigger CreditApplicationTrigger on dealer__Credit_Application__c (after delete, after insert, after undelete, after update, before delete, before insert, before update)
{
	TriggerTemplate.TriggerManager triggerManager = new TriggerTemplate.TriggerManager();
	triggerManager.addHandler(new CreditApplicationHandler(), new List<TriggerTemplate.TriggerAction>{
			TriggerTemplate.TriggerAction.afterUpdate, TriggerTemplate.TriggerAction.afterInsert, 
			TriggerTemplate.TriggerAction.beforeUpdate, TriggerTemplate.TriggerAction.beforeInsert
	});

	triggerManager.runHandlers();
}